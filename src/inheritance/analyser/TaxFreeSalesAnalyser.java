package inheritance.analyser;

import java.util.List;

public class TaxFreeSalesAnalyser extends GeneralCalculator {

    @Override
    public double percents(SalesRecord item){
        return 1.0;
    }

    public TaxFreeSalesAnalyser(List<SalesRecord> records) {
        super(records);
        //throw new RuntimeException("not implemented yet");
    }

}
