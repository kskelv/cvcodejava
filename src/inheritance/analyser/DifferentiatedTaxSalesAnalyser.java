package inheritance.analyser;
//20+10
import java.util.List;

public class DifferentiatedTaxSalesAnalyser extends GeneralCalculator {

    @Override
    public double percents(SalesRecord item){
        if (item.hasReducedRate()) {
            return 1.1;
        } else {
            return 1.2;
        }
    }

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);

    }
}
