package inheritance.analyser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class GeneralCalculator {

    public abstract double percents(SalesRecord item);

    private List<SalesRecord> records;

    public GeneralCalculator(List<SalesRecord> records) {
        this.records = records;
    }

    public Double getTotalSales() {
        /*
        метод который проходя по листу
        создает total
        total это цена продукта / на процент (чтобы избавиться от налога) * на количество этого товара
        получаем в итоге доход от продукта без налога
         */
        double count = 0.0;
        for (SalesRecord item: records){
            double total = (item.getProductPrice() / percents(item)) * item.getItemsSold();
            count += total;
        }
        return count;
    }

    public Double getTotalSalesByProductId(String id) {
        /*
        метод который проходя по листу
        if ищет одинаковые ID
        создает total
        total это цена продукта / на процент (чтобы избавиться от налога) * на количество этого товара
        получаем в итоге доход от продукта без налога
        count - если ID продукта уже встречался то count считает их доход вместе
         */
        double count = 0.0;
        for (SalesRecord item: records){
            if (item.getProductId().equals(id)){
                double total = (item.getProductPrice() / percents(item)) * item.getItemsSold();
                count += total;
            }
        }
        return count;
    }

    private HashMap<String, List<Integer>> getDict() {

        HashMap<String, List<Integer>> newDictOfRecords = new HashMap<>();
        //создаем словарь где ID это ключ а лист с PRICE и SOLD это значение

        for (SalesRecord item: records){
            if (newDictOfRecords.containsKey(item.getProductId())){

                List<Integer> getPriceAndSold = new ArrayList<>();
                //создаем лист - значение словаря

                getPriceAndSold.add(item.getProductPrice());
                getPriceAndSold.add(newDictOfRecords.get(item.getProductId()).get(1) + item.getItemsSold());
                /*
                добавляем в лист PRICE
                добавляем SOLD
                1. эй словарь дай мне значение по такому то ключу
                2. а из этого значения которое есть лист я хочу второй элемент
                3. и прибавь к нему еще SOLD всех тех продуктов у которых такой же ID
                 */

                newDictOfRecords.replace(item.getProductId(), newDictOfRecords.get(item.getProductId()), getPriceAndSold);
                //а теперь возьми вон тот ключ, его первоначальное значение и поменяй его вот на тот кошмар что мы написали в листе выше
            }

            if (!newDictOfRecords.containsKey(item.getProductId())){
                newDictOfRecords.put(item.getProductId(), List.of(item.getProductPrice(), item.getItemsSold()));
                //ну а если у нас нет такого ключа в словаре то добавь его и его значение
            }

    }
        return newDictOfRecords;
    }

    public String getIdOfMostPopularItem() {
        int maxCount = 0;
        String idMaxCount = "";

        HashMap<String, List<Integer>> getDict = getDict();
        //вызаваем словарь

        for (SalesRecord i : records){
           if (maxCount < getDict.get(i.getProductId()).get(1)){
               maxCount = getDict.get(i.getProductId()).get(1);
               idMaxCount = i.getProductId();

               /*
               если второй элемент листа (который значение по ключу в словаре) больше maxCount
               то maxCount становится этим значением
               а idMaxCount это имя продукта с самым большим колличеством
                */
           }
        }
        return idMaxCount;
    }


    public String getIdOfItemWithLargestTotalSales() {
        double maxCount = 0;
        String idMaxCount = "";

        HashMap<String, List<Integer>> getDict = getDict();

        for (SalesRecord i : records){
            if (maxCount < getDict.get(i.getProductId()).get(0) * getDict.get(i.getProductId()).get(1) / percents(i)) {
                maxCount = getDict.get(i.getProductId()).get(0) * getDict.get(i.getProductId()).get(1) / percents(i);
                idMaxCount = i.getProductId();

               /*
               если первый элемент листа * на второй элемент листа (которые значение по ключу в словаре) / на метод убирающий налог больше maxCount
               то maxCount становится этим значением
               а idMaxCount это имя продукта с самым большим доходом
                */
            }
        }

        return idMaxCount;
    }
}
