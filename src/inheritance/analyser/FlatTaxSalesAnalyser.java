package inheritance.analyser;
//20
import java.util.List;

public class FlatTaxSalesAnalyser extends GeneralCalculator {

    @Override
    public double percents(SalesRecord item){
        return 1.2;
    }

    public FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
        //throw new RuntimeException("not implemented yet");
    }

}
