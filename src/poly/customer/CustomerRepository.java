package poly.customer;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CustomerRepository {

    List<List<String>> data = new ArrayList<>();
    public List<AbstractCustomer> customerList;

    private static final String FILE_PATH = "src/poly/customer/data.txt";
    private DateTimeFormatter dateType = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public CustomerRepository() {

        try{
            List<String> readFile = Files.readAllLines(Paths.get(FILE_PATH));
//весь файл это лист, каждая строчка это стринг
            data = readFile
                    .stream()
                    .map(x -> Arrays.asList(x.split(";")))
//сплитим каждую строчку по ;
                    .collect(Collectors.toList());
/*
   data =
 [
    ["REGULAR", "c1", "Alice", "0", "2020-03-10"],
    ["REGULAR", "c2", "Bob", "0", "2020-01-04"],
    ["GOLD", "c3", "Carol", "0", ""]
 ]
 */

            customerList = data
                    .stream()
                    .map(this::goldOrRegular)
                    .collect(Collectors.toList());


        } catch (IOException e){
            System.out.println(e);
        }
    }

    private AbstractCustomer goldOrRegular(List<String> data){

        String id = data.get(1);
        String name = data.get(2);
        int bonusPoints = Integer.parseInt(data.get(3));



        if (data.get(0).equals("GOLD")){
            return new GoldCustomer(id, name, bonusPoints);
        }
        else if (data.get(0).equals("REGULAR")){
            return new RegularCustomer(id, name, bonusPoints, LocalDate.parse(data.get(4), dateType));
        }
        else {
            throw new RuntimeException();
        }
    }

    public Optional<AbstractCustomer> getCustomerById(String id) {
        for (AbstractCustomer customer : customerList) {
            if (customer.getId().equals(id)) {
                return Optional.of(customer);
            }
        }
        return Optional.empty();
    }

    public void remove(String id) {
        customerList.removeIf(customer -> customer.getId().equals(id));
    }
// написал for и он переделал мне его в это

    public void save(AbstractCustomer customer) {
        remove(customer.getId());
        customerList.add(customer);

        try{
            FileWriter newData = new FileWriter(FILE_PATH);
            String custString = customerList
                    .stream()
                    .map(AbstractCustomer::asString)
                    .collect(Collectors.joining("\n"));
            newData.write(custString);
            newData.close();
        } catch (Exception e){
            System.out.println(e);
        }

    }

    public int getCustomerCount() {
        return customerList.size();

    }


}

