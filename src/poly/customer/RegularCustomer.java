package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public class RegularCustomer extends AbstractCustomer {

    private LocalDate lastOrderDate;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);
        this.lastOrderDate = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {

        if (order.getTotal() < 100){
            return;
        }
        if (order.getDate().compareTo(lastOrderDate.plusMonths(1)) <= 0){
            bonusPoints += order.getTotal() * 1.5;
        }
        else {
            bonusPoints += order.getTotal() * 1;
        }
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        RegularCustomer other = (RegularCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        throw new RuntimeException("not implemented yet");
    }

    @Override
    public String asString() {
        return String.format("REGULAR;%s;%s;%s;%s;", id, name, bonusPoints, lastOrderDate);
    }

}