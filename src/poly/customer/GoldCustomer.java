package poly.customer;

import java.util.Objects;

public class GoldCustomer extends AbstractCustomer {

    public GoldCustomer(String id, String name,
                        int bonusPoints) {

        super(id, name, bonusPoints);
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() >= 100) {
            bonusPoints += order.getTotal() * 1.5;
        }
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        GoldCustomer other = (GoldCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        throw new RuntimeException("not implemented yet");
    }

    @Override
    public String asString() {
        return String.format("GOLD;%s;%s;%s;", id, name, bonusPoints);

    }

}