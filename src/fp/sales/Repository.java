package fp.sales;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    public List<List<String>> pes;
    public List<Entry> entries;

    public Repository(){
        try{
            List<String> gog = Files.readAllLines(Paths.get(FILE_PATH));
            pes = gog
                    .stream()
                    .map(x -> Arrays.asList(x.split("\t")))
                    .collect(Collectors.toList());
            pes.remove(0);

        } catch (IOException e){
            System.out.println(e);
        }

        entries = pes.stream()
                .map(x -> new Entry(x))
                .collect(Collectors.toList());

    }
}
