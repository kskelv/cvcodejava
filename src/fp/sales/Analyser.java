package fp.sales;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

public class Analyser {

    private Repository repository;

    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {

        return repository.entries.stream()
                .mapToDouble(entry -> entry.getAmount()).sum();
    }

    public Double getSalesByCategory(String category) {

        return repository.entries.stream()
                .filter(entry -> entry.getCategory().equals(category))
                .mapToDouble(entry -> entry.getAmount()).sum();
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {

        return repository.entries.stream()
                .filter(entry -> entry.getDate().compareTo(start) >= 0 && entry.getDate().compareTo(end) <= 0)
                .mapToDouble(entry -> entry.getAmount()).sum();
    }

    public String mostExpensiveItems() {
        return repository.entries.stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(entry -> entry.getAmount())))
                .limit(3)
                .collect(Collectors.toList())
                .stream()
                .sorted(Comparator.comparing(Entry::getProductId))
                .map(Entry::getProductId)
                .collect(Collectors.joining(", "));

    }

    public String statesWithBiggestSales() {

        Map<String, Double> gog = repository.entries
                .stream()
                .collect(Collectors.toMap(
                        Entry::getState,
                        Entry::getAmount,
                        Double::sum));

        return gog.keySet().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(gog::get)))
                .limit(3)
                .collect(Collectors.joining(", "));

    }
}
