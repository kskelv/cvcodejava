package fp.sales;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Entry {

    private String productId;
    private LocalDate date;
    private String state;
    private String category;
    private Double amount;


    public Entry(List<String> listOfStr){
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("dd.MM.yyyy");

        date = LocalDate.parse(listOfStr.get(0), formatter);
        amount = Double.parseDouble(listOfStr.get(5)
                .replace(",","."));

        state = listOfStr.get(1);
        productId = listOfStr.get(2);
        category = listOfStr.get(3);

    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
