package generics.cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShoppingCart<T extends CartItem> {
    ArrayList<T> cart = new ArrayList<>();
    ArrayList<Double> discount = new ArrayList<>();

    public void add(T item) {
        cart.add(item);
    }

    public void removeById(String id) {
        cart.removeIf(obj -> obj.getId().equals(id));
    }

    public Double getTotal() {
        double count = 0.0;
        for (T obj : cart){
            double total = obj.getPrice();
            count += total;
        }
        // что то страшное
        Double biggerDiscount = 1.0;
        for (Double percent : discount){
            biggerDiscount *= percent;
        }
        return count * biggerDiscount;
    }

    public String toString(){

        StringBuilder result = new StringBuilder();

        HashMap<String, Integer> dictOfItem = new HashMap<>();
        for (T obj : cart){
            if (dictOfItem.containsKey(obj.getId())){
                dictOfItem.replace(obj.getId(), dictOfItem.get(obj.getId()), dictOfItem.get(obj.getId())+1);
            } else {
                dictOfItem.put(obj.getId(), 1);
            }
        }
        for (String object : List.copyOf(dictOfItem.keySet())) {
            result.append(String.format("(%s, %s, %s)", object, getIdPrice(object), dictOfItem.get(object)));
            if (List.copyOf(dictOfItem.keySet()).indexOf(object) != dictOfItem.keySet().size()-1){
                result.append(", ");
            }
        }
        return result.toString();
    }

    private Double getIdPrice(String id){
        for (T obj : cart){
            if (obj.getId().equals(id)){
                return obj.getPrice();
            }
        }
        throw new RuntimeException("Ty pes gavna");
    }

    public void increaseQuantity(String id) {
        for (T obj : cart){
            if (obj.getId().equals(id)){
                cart.add(obj);
                break;
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        this.discount.add((100 - discount) / 100);
    }

    public void removeLastDiscount() {
        if(!discount.isEmpty()){
            discount.remove(discount.size()-1);
        }
    }

    public void addAll(List<T> items) {
        for (T obj : items) {
            add(obj);
        }
    }
}
