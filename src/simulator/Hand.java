package simulator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Hand implements Iterable<Card> {

    private List<Card> cards = new ArrayList<>();

    private ArrayList<Integer> cardValues = new ArrayList<>();

    private HandType high = HandType.HIGH_CARD;

    public void addCard(Card card) {
        cards.add(card);
        cardValues.add(Card.valueToNumber(card.getValue().toString()));
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        System.out.println(cardValues);

        List<HandType> names = List.of(straightFlush(), care(), fullHouse(), flush(), straight(), sett(), twoCouples(), couple());
        for (HandType name : names) {
            if (name != high) {
                return name;
            }
        }
        return HandType.HIGH_CARD;
    }

    private HandType straightFlush() {
        if (cards.size() == 5) {
            for (int i = 1; i < cards.size(); i++) {
                if (cards.get(0).getSuit() != cards.get(i).getSuit()) {
                    return high;
                }
                Collections.sort(cardValues);
                for (int j = 1; j < cardValues.size(); j++) {
                    if (cardValues.get(0) != cardValues.get(j) - j) {
                        return high;

                    }
                }
            }
        return HandType.STRAIGHT_FLUSH;
        }
        return high;
    }

    private HandType care() {
        if (cards.size() >= 4) {
            Collections.sort(cardValues);
            for (Integer value : cardValues) {
                if (Collections.frequency(cardValues, value) == 4) {

                    return HandType.FOUR_OF_A_KIND;
                }
            }
        }
        return high;
    }

    private HandType couple() {
        if (cards.size() >= 2) {
            Collections.sort(cardValues);
            for (Integer value : cardValues) {
                if (Collections.frequency(cardValues, value) == 2) {

                    return HandType.ONE_PAIR;
                }
            }
        }
        return high;
    }

    private HandType twoCouples() {
        Collections.sort(cardValues);
        if (cards.size() >= 4 && Collections.frequency(cardValues, cardValues.get(1)) == 2 && Collections.frequency(cardValues, cardValues.get(3)) == 2) {
            return HandType.TWO_PAIRS;
        }
        return high;
    }

    private HandType sett() {
        if (cards.size() >= 3) {
            Collections.sort(cardValues);
            for (Integer value : cardValues) {
                if (Collections.frequency(cardValues, value) == 3) {
                    return HandType.TRIPS;
                }
            }
        }
        return high;
    }

    private HandType straight() {
        if (cards.size() == 5) {
            ArrayList<Integer> pesGavna = for2InHand();
            Collections.sort(pesGavna);
            for (int j = 1; j < pesGavna.size(); j++) {
                if (pesGavna.get(0) != pesGavna.get(j) - j) {
                    return high;
                }
            }
        return HandType.STRAIGHT;
        }
        return high;
    }

    private ArrayList<Integer> for2InHand(){
        ArrayList<Integer> pesGavna = new ArrayList<>();
            for (Integer element : cardValues) {
                if (element == 14 && cardValues.contains(2)) {
                    pesGavna.add(1);
                } else {
                    pesGavna.add(element);
                }
            }
        return pesGavna;
    }

    private HandType flush() {
        if (cards.size() == 5) {
            for (int i = 1; i < cards.size(); i++) {
                if (cards.get(0).getSuit() != cards.get(i).getSuit()) {
                    return high;
                }
            }
        return HandType.FLUSH;
        }
        return high;
    }

    private HandType fullHouse() {
    Collections.sort(cardValues);
        if (cards.size() >= 5 && (
                Collections.frequency(cardValues, cardValues.get(0)) == 3 &&
                Collections.frequency(cardValues, cardValues.get(4)) == 2 ||
                Collections.frequency(cardValues, cardValues.get(0)) == 2 &&
                Collections.frequency(cardValues, cardValues.get(4)) == 3)) {
            return HandType.FULL_HOUSE;
        }

        return high;
    }


    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
