package simulator;

import java.util.Map;
import java.util.Objects;

public class Card implements Comparable<Card> {

    public enum CardValue { S2, S3, S4, S5, S6, S7, S8, S9, S10, J, Q, K, A }

    public enum CardSuit { C, D, H, S }

    private final CardValue value;
    private final CardSuit suit;

    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card)) {
            return false;
        }

        Card other = (Card) obj;

        return Objects.equals(value, other.value)
                && Objects.equals(suit, other.suit);
    }


    public static Integer valueToNumber(String value){
        Map<String, Integer> dictOfCardValues = Map.of(
                "J", 11, "Q", 12, "K", 13, "A", 14
        );
        if (dictOfCardValues.containsKey(value)){
            return dictOfCardValues.get(value);
        } else {
            String gog = value.substring(1);
            return Integer.valueOf(gog);
        }

    }

    @Override
    public int compareTo(Card other) {
        String pesValue = this.value.toString();
        String otherValue = other.value.toString();
        Integer ourCardValueInNum = valueToNumber(pesValue);
        Integer otherCardValueInNum = valueToNumber(otherValue);

        return ourCardValueInNum.compareTo(otherCardValueInNum);
    }

    public CardValue getValue() {
        return value;
    }

    public CardSuit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", value, suit);
    }
}
