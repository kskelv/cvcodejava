package simulator;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.*;

import static simulator.Card.CardSuit.*;
import static simulator.Card.CardValue.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimulatorTests {

    @Test
    public void getHandIsHelperMethodForCreatingHandWithTheSpecifiedCards() {
        Hand hand = getHand("AK"); // suits are arbitrary

        assertThat(hand.toString(), is("[(A, D), (K, C)]"));
    }

    @Test
    public void handKnowsWhetherItContainsOnePair() {
        assertThat(getHand("AA").getHandType(),
                is(HandType.ONE_PAIR));

        assertThat(getHand("AK").getHandType(),
                is(not(HandType.ONE_PAIR)));

        assertThat(getHand("AAKK").getHandType(),
                is(not(HandType.ONE_PAIR)));

        assertThat(getHand("AAA").getHandType(),
                is(not(HandType.ONE_PAIR)));

        assertThat(getHand("KKAAA").getHandType(),
                is(not(HandType.ONE_PAIR)));
    }

    @Test
    public void handKnowsWhetherItContainsTwoPairs() {
        assertThat(getHand("AAKK").getHandType(),
                is(HandType.TWO_PAIRS));

        assertThat(getHand("AAKKK").getHandType(),
                is(not(HandType.TWO_PAIRS)));
    }

    @Test
    public void handKnowsWhetherItContainsTrips() {
        assertThat(getHand("AAA").getHandType(),
                is(HandType.TRIPS));

        assertThat(getHand("AAKKK").getHandType(),
                is(not(HandType.TRIPS)));
    }

    @Test
    public void handKnowsWhetherItContainsStraight() {
        assertThat(getHand("A2345").getHandType(),
                is(HandType.STRAIGHT));

        assertThat(getHand("TJQKA").getHandType(),
                is(HandType.STRAIGHT));

        assertThat(getHand("2345").getHandType(),
                is(not(HandType.STRAIGHT)));

        assertThat(getHand("23567").getHandType(),
                is(not(HandType.STRAIGHT)));

        assertThat(getHand("JQKA2").getHandType(),
                is(not(HandType.STRAIGHT)));
    }

    @Test
    public void handKnowsWhetherItContainsFlush() {
        assertThat(getFlushHand("23567").getHandType(),
                is(HandType.FLUSH));

        assertThat(getHand("23456").getHandType(),
                is(not(HandType.FLUSH)));

        assertThat(getFlushHand("23456").getHandType(),
                is(not(HandType.FLUSH)));
    }

    @Test
    public void handKnowsWhetherItContainsFullHouse() {
        assertThat(getFlushHand("AKAAK").getHandType(),
                is(HandType.FULL_HOUSE));

        assertThat(getHand("AAAAK").getHandType(),
                is(not(HandType.FULL_HOUSE)));
    }

    @Test
    public void handKnowsWhetherItContainsFourOfAKind() {
        assertThat(getHand("AAAA").getHandType(),
                is(HandType.FOUR_OF_A_KIND));
    }

    @Test
    public void handKnowsWhetherItContainsStraightFlush() {
        assertThat(getFlushHand("23456").getHandType(),
                is(HandType.STRAIGHT_FLUSH));

        assertThat(getHand("23456").getHandType(),
                is(not(HandType.STRAIGHT_FLUSH)));

        assertThat(getFlushHand("23567").getHandType(),
                is(not(HandType.STRAIGHT_FLUSH)));
    }

    @Test
    public void cardsCanBeCompared() {
        assertThat(new Card(A, C).compareTo(new Card(A, S)), is(0));
        assertThat(new Card(A, C).compareTo(new Card(K, S)), is(1));
        assertThat(new Card(K, H).compareTo(new Card(A, H)), is(-1));
    }

    @Test
    public void cardsCanBeSorted() {
        List<Card> cards = Arrays.asList(
                new Card(K, H),
                new Card(Q, H),
                new Card(J, H),
                new Card(S10, S),
                new Card(A, C));

        Collections.sort(cards);

        assertThat(cards.toString(),
                is("[(S10, S), (J, H), (Q, H), (K, H), (A, C)]"));
    }

    @Test // if this test fails then max points are 4 instead of 5
    public void calculatesProbabilitiesUsingSimulation() {

        Simulator simulator = new Simulator(3e5);

        Map<HandType, Double> map = simulator.calculateProbabilities();


        // https://en.wikipedia.org/wiki/Poker_probability#Frequency_of_5-card_poker_hands

        assertThat(map.get(HandType.HIGH_CARD), is(closeTo(50.1)));
        assertThat(map.get(HandType.ONE_PAIR), is(closeTo(42.3)));
        assertThat(map.get(HandType.TWO_PAIRS), is(closeTo(4.8)));
        assertThat(map.get(HandType.TRIPS), is(closeTo(2.1)));
    }

    private Hand getHand(String cardValues) {
        Map<Character, Card.CardValue> map = new HashMap<>();

        for (Card.CardValue value : Card.CardValue.values()) {
            int index = value.ordinal();
            map.put("23456789TJQKA".charAt(index), value);
        }

        Hand hand = new Hand();
        for (char character : cardValues.toCharArray()) {
            Card.CardValue cardValue = map.get(character);
            Card.CardSuit suit = getAvailableSuitFor(cardValue, hand);
            hand.addCard(new Card(cardValue, suit));
        }
        return hand;
    }

    private Hand getFlushHand(String cardValues) {
        Hand hand = new Hand();
        for (Card each : getHand(cardValues)) {
            hand.addCard(new Card(each.getValue(), Card.CardSuit.S));
        }

        return hand;
    }

    private Card.CardSuit getAvailableSuitFor(Card.CardValue cardValue, Hand hand) {
        if (hand.isEmpty()) {
            return Card.CardSuit.D;
        }

        for (Card.CardSuit suit : Card.CardSuit.values()) {
            if (!hand.contains(new Card(cardValue, suit))) {
                return suit;
            }
        }

        throw new IllegalArgumentException(
                String.format("no suit available for %s in hand %s",
                        cardValue, hand));
    }

    private Matcher<Double> closeTo(double value) {
        double precision = 0.2;

        return Matchers.closeTo(value, precision);
    }
}
