package exceptions.numbers;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {

    public final String lang;
    private Properties propertiesDict = new Properties();

    public NumberConverter(String lang) {

        this.lang = lang;

        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);
            InputStreamReader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            propertiesDict.load(reader);

        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException(lang, e);

        } catch (Exception e) {
            throw new MissingLanguageFileException(lang, e);

        } finally {
            close(is);
        }
    }

    public String numberInWords(Integer number) {
        String filePath = String.format("src/exceptions/numbers/expected-%s.txt", lang);

        File file = new File(filePath);

        if (file.length() == 0){
        throw new MissingTranslationException(lang);
        }

        String numInWord = "";
        int hunt = number / 100;
        int tens = (number / 10) % 10;
        int ones = number % 10;


        String onesInWord = onesInWord(ones, tens, hunt);
        String tensInWord = tensInWord(tens, number, ones);
        String huntInWord = huntInWord(hunt, tens, ones);


        if (hunt > 0){
            numInWord += huntInWord;
        }

        if (tens == 1){
            numInWord += tensInWord;
        }

        if (tens > 1){
            numInWord += tensInWord + onesInWord;
        }

        if (tens == 0){
            numInWord += onesInWord;
        }

        return numInWord;
    }


    public String onesInWord(int ones, int tens, int hunt){
        String onesInWord = "";
        if (ones != 0) {
            onesInWord += propertiesDict.getProperty(String.valueOf(ones));
        }
        if (ones == 0 && tens == 0 && hunt == 0) {
            onesInWord += propertiesDict.getProperty(String.valueOf(ones));
        }
        return onesInWord;
    }


    public String tensInWord(int tens, int number, int ones){
        String tensInWord = "";
        if (tens == 1){
            if (!propertiesDict.containsKey(String.valueOf(number%100))){
                tensInWord += propertiesDict.getProperty(String.valueOf(ones)) + propertiesDict.getProperty("teen");
            }
            if (propertiesDict.containsKey(String.valueOf(number%100))){
                tensInWord += propertiesDict.getProperty(String.valueOf(number%100));
            }
        }

        if (tens > 1){
            tensInWord += tensInWordSecondIf(tens, ones, tensInWord);
        }
        return tensInWord;
    }
    public String tensInWordSecondIf(int tens, int ones, String tensInWord){

        if (!propertiesDict.containsKey(String.valueOf(tens*10)) && ones > 0){
            tensInWord += propertiesDict.getProperty(String.valueOf(tens)) + propertiesDict.getProperty("tens-suffix") + propertiesDict.getProperty("tens-after-delimiter");
        }
        if (propertiesDict.containsKey(String.valueOf(tens*10)) && ones > 0){
            tensInWord += propertiesDict.getProperty(String.valueOf(tens*10)) + propertiesDict.getProperty("tens-after-delimiter");
        }
        if (!propertiesDict.containsKey(String.valueOf(tens*10)) && ones == 0){
            tensInWord += propertiesDict.getProperty(String.valueOf(tens)) + propertiesDict.getProperty("tens-suffix");
        }
        if (propertiesDict.containsKey(String.valueOf(tens*10)) && ones == 0){
            tensInWord += propertiesDict.getProperty(String.valueOf(tens*10));
        }
        return tensInWord;
    }

     public String huntInWord(int hunt, int tens, int ones){

         String huntInWord = "";
         if (hunt > 0 && tens == 0 && ones == 0) {
             huntInWord +=
                     propertiesDict.getProperty(String.valueOf(hunt)) +
                             propertiesDict.getProperty("hundreds-before-delimiter") +
                             propertiesDict.getProperty("hundred");
         }
         if (hunt > 0 && (tens != 0 || ones != 0)) {
             huntInWord +=
                     propertiesDict.getProperty(String.valueOf(hunt)) +
                             propertiesDict.getProperty("hundreds-before-delimiter") +
                             propertiesDict.getProperty("hundred") +
                             propertiesDict.getProperty("hundreds-after-delimiter");
         }
         return huntInWord;
     }


    private static void close(FileInputStream is) {
        if (is == null) {
            return;
        }
        try {
            is.close();
        } catch (IOException ignore) {}
    }

}
